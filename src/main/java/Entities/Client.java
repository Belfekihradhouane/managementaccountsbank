package Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Objects;
@Entity
public class Client implements Serializable {
    //code client
    @Id
    @GeneratedValue
    private  Long codeClient;
    private String nom;
    private String email;
    //un client peut avoir plusieurs comptes
    @OneToMany(mappedBy = "client",fetch = FetchType.LAZY)
    private Collection<Compte> comptes;

    public Client() {
    }

    public Client(String nom, String email) {
        this.nom = nom;
        this.email = email;
    }

    public Long getCodeClient() {
        return codeClient;
    }

    public String getNom() {
        return nom;
    }

    public String getEmail() {
        return email;
    }

    public Collection<Compte> getComptes() {
        return comptes;
    }

    public void setCodeClient(Long codeClient) {
        this.codeClient = codeClient;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setComptes(Collection<Compte> comptes) {
        this.comptes = comptes;
    }

    @Override
    public String toString() {
        return "Client{" +
                "codeClient=" + codeClient +
                ", nom='" + nom + '\'' +
                ", email='" + email + '\'' +
                ", comptes=" + comptes +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return Objects.equals(codeClient, client.codeClient) &&
                Objects.equals(nom, client.nom) &&
                Objects.equals(email, client.email) &&
                Objects.equals(comptes, client.comptes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codeClient, nom, email, comptes);
    }
}
