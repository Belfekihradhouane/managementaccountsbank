package Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;
@Entity
public class Compte implements Serializable {
@Id
private  String codeCompte;
private Date dateCreation;
private Double solde;
private double decouvert;
// plusieurs comptes sont  associés à un seul client
@ManyToOne
@JoinColumn(name="CODE_CLIENT")
private Client client;
//  Un compte peut avoir Plusieurs opérations
@OneToMany(mappedBy = "compte")
private Collection<Operation> operations;

    public Compte() {
    }

    public Compte(String codeCompte, Date dateCreation, Double solde, double decouvert, Client client) {
        this.codeCompte = codeCompte;
        this.dateCreation = dateCreation;
        this.solde = solde;
        this.decouvert = decouvert;
        this.client = client;
    }

    public String getCodeCompte() {
        return codeCompte;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public Double getSolde() {
        return solde;
    }

    public double getDecouvert() {
        return decouvert;
    }

    public Client getClient() {
        return client;
    }

    public Collection<Operation> getOperations() {
        return operations;
    }

    public void setCodeCompte(String codeCompte) {
        this.codeCompte = codeCompte;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setSolde(Double solde) {
        this.solde = solde;
    }

    public void setDecouvert(double decouvert) {
        this.decouvert = decouvert;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setOperations(Collection<Operation> operations) {
        this.operations = operations;
    }

    @Override
    public String toString() {
        return "Compte{" +
                "codeCompte='" + codeCompte + '\'' +
                ", dateCreation=" + dateCreation +
                ", solde=" + solde +
                ", decouvert=" + decouvert +
                ", client=" + client +
                ", operations=" + operations +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Compte compte = (Compte) o;
        return Double.compare(compte.decouvert, decouvert) == 0 &&
                Objects.equals(codeCompte, compte.codeCompte) &&
                Objects.equals(dateCreation, compte.dateCreation) &&
                Objects.equals(solde, compte.solde) &&
                Objects.equals(client, compte.client) &&
                Objects.equals(operations, compte.operations);
    }

    @Override
    public int hashCode() {
        return Objects.hash(codeCompte, dateCreation, solde, decouvert, client, operations);
    }
}
