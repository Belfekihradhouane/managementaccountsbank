package Entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="TYPE_OP",discriminatorType = DiscriminatorType.STRING,length = 1)
public abstract class  Operation implements Serializable {
    @Id
    @GeneratedValue
    private Long numero;
    private Date dateOperation;
    private  Double montant;
    @ManyToOne
    @JoinColumn(name="CODE_COMPTE")
    private Compte compte;

    public Operation() {
    }

    public Operation(Date dateOperation, Double montant, Compte compte) {
        this.dateOperation = dateOperation;
        this.montant = montant;
        this.compte = compte;
    }

    public Long getNumero() {
        return numero;
    }

    public Date getDateOperation() {
        return dateOperation;
    }

    public Double getMontant() {
        return montant;
    }

    public Compte getCompte() {
        return compte;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public void setDateOperation(Date dateOperation) {
        this.dateOperation = dateOperation;
    }

    public void setMontant(Double montant) {
        this.montant = montant;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }

    @Override
    public String toString() {
        return "Operation{" +
                "numero=" + numero +
                ", dateOperation=" + dateOperation +
                ", montant=" + montant +
                ", compte=" + compte +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Operation operation = (Operation) o;
        return Objects.equals(numero, operation.numero) &&
                Objects.equals(dateOperation, operation.dateOperation) &&
                Objects.equals(montant, operation.montant) &&
                Objects.equals(compte, operation.compte);
    }

    @Override
    public int hashCode() {
        return Objects.hash(numero, dateOperation, montant, compte);
    }
}
