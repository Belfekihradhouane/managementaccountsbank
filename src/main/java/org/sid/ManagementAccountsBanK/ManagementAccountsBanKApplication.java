package org.sid.ManagementAccountsBanK;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ManagementAccountsBanKApplication {

	public static void main(String[] args) {
		SpringApplication.run(ManagementAccountsBanKApplication.class, args);
	}

}
